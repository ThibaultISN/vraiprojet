CREATE TABLE magasin (
  nom varchar(20),
  region varchar(20),
  departement varchar(30),
  ville varchar(20),
  adresse varchar(40) PRIMARY KEY,
  numTel varchar(10)
);

CREATE TABLE produit (
  ref varchar(3) PRIMARY KEY,
  nom varchar(20),
  categorie TEXT,
  prix REAL
);
