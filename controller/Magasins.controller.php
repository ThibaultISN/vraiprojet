<?php
  include_once("../model/DAO.class.php");
  include_once("../model/Magasin.class.php");

  $dao = new DAO();

  $regions = $dao->getToutesLesRegions();
  $departements = $dao->getTousLesDep();

  if (isset($_GET['region'])) {
    $reg = $_GET['region'];
  }

  if (isset($_GET['departement'])) {
    $dep = $_GET['departement'];
  }

  if (isset($_GET['region']) && isset($_GET['departement'])) {
    $magasins = $dao->getMagasins($reg,$dep);
  } else {
    $magasins = $dao->getTousLesMagasins();
  }

  include("../view/Magasins.view.php");

?>
