<?php
  include_once("../model/DAO.class.php");
  include_once("../model/Produit.class.php");

  $dao = new DAO();

  $filters = $dao->getCategories();

  $def = false;
  $produits = array();
  $queryFilters = "";

  foreach ($filters as $filter) {
    if (isset($_GET[$filter])) {
      $queryFilters .= "&".$filter."=on";
      $prods = $dao->getProduits($filter);
      foreach ($prods as $prod) {
        $produits[] = $prod;
      }
      $def = true;
    }
  }
  if ($def == false) {
    $produits = $dao->getTousLesProduits();
  }

  $liste = array();

  if (isset($_GET['maListe'])) {
    $str = $_GET['maListe'];
    $li = explode(",", $str);
    foreach ($li as $elem) {
      $liste[] = $dao->getProduit($elem);
    }
  }

  if (isset($_GET['ajoutListe'])) {
    $reference = $_GET['ajoutListe'];
    $liste[] = $dao->getProduit($reference);
  }

  $listeRefs = array();
  foreach ($liste as $elem) {
    $listeRefs[] = $elem->getRef();
  }

  $listeRefs = array_unique($listeRefs);

  unset($liste);
  $liste = array();

  foreach ($listeRefs as $ref) {
    $liste[] = $dao->getProduit($ref);
  }

  $queryMaListe = "";
  foreach ($liste as $elem) {
    $queryMaListe .= ','.$elem->getRef();
  }

  $queryMaListe = substr($queryMaListe,1);

  include("../view/Boutique.view.php");
?>
