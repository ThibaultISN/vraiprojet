<?php
/**
 *
 */
class Produit
{
  private $ref;
  private $nom;
  private $categorie;
  private $prix;

  function __construct(string $ref = "null", string $nom = "null", string $categorie = "null", float $prix = 0.0)
  {
    $this->ref = $ref;
    $this->nom = $nom;
    $this->categorie = $categorie;
    $this->prix = $prix;
  }

  function getRef() : string
  {
    return $this->ref;
  }

  function getNom() : string
  {
    return $this->nom;
  }

  function getCategorie() : string
  {
    return $this->categorie;
  }

  function getPrix() : float
  {
    return $this->prix;
  }
}

 ?>
