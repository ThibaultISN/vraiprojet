<?php

include_once("../model/Produit.class.php");
include_once("../model/Magasin.class.php");

class DAO
{
  private $db;
  private $database = 'sqlite:../data/houblonstore.db';

  function __construct()
  {
    try {
      $this->db = new PDO($this->database);
    }
    catch (PDOException $e){
      die("Erreur de connexion:".$e->getMessage());
    }
  }

  function getToutesLesRegions() : array {
    $req = "select distinct region from magasin";
    $sth = ($this->db)->query($req);
    $regions = $sth->fetchAll(PDO::FETCH_NUM);
    return $regions;
  }

  function getTousLesDep() : array {
    $req = "select distinct departement from magasin";
    $sth = ($this->db)->query($req);
    $departements = $sth->fetchAll(PDO::FETCH_NUM);
    return $departements;
  }

  function getMagasins(string $reg, string $dep) : array
  {
    $req = "select * from magasin where region='$reg' and departement='$dep'";
    $sth = ($this->db)->query($req);
    $magasins = $sth->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Magasin');
    return $magasins;
  }

  function getTousLesMagasins() : array
  {
    $req = "select * from magasin";
    $sth = ($this->db)->query($req);
    $magasins = $sth->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Magasin');
    return $magasins;
  }

  function getCategories() : array {
    $req = "select distinct categorie from produit";
    $sth = ($this->db)->query($req);
    $cat = $sth->fetchAll(PDO::FETCH_NUM);
    $categories = array();
    foreach ($cat as $value) {
      $cats = explode("/",$value[0]);
      foreach ($cats as $val) {
        $categories[] = $val;
      }
    }
    $categories = array_unique($categories);
    return $categories;
  }

  function getProduits(string $categorie) : array
  {
    $req = "select * from produit where categorie like '%$categorie%'";
    $sth = ($this->db)->query($req);
    $produits = $sth->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Produit');
    return $produits;
  }

  function getTousLesProduits() : array
  {
    $req = "select * from produit";
    $sth = ($this->db)->query($req);
    $produits = $sth->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Produit');
    return $produits;
  }

  function getProduit(string $ref) : Produit {
    $req = "select * from produit where ref='$ref'";
    $sth = ($this->db)->query($req);
    $produits = $sth->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,'Produit');
    return $produits[0];
  }

}

?>
