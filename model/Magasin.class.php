<?php
/**
 *
 */
class Magasin
{
  private $nom;
  private $region;
  private $departement;
  private $ville;
  private $adresse;
  private $numTel;

  function __construct(string $nom = "null", string $region = "null", string $departement = "null", string $ville = "null", string $adresse = "null", string $numTel = "null")
  {
    $this->nom = $nom;
    $this->region = $region;
    $this->departement = $departement;
    $this->ville = $ville;
    $this->adresse = $adresse;
    $this->numTel = $numTel;
  }

  function getNom() : string
  {
    return $this->nom;
  }

  function getRegion() : string
  {
    return $this->region;
  }

  function getDepartement() : string
  {
    return $this->departement;
  }

  function getVille() : string
  {
    return $this->ville;
  }

  function getAdresse() : string
  {
    return $this->adresse;
  }

  function getNumTel() : string
  {
    return $this->numTel;
  }
}

 ?>
