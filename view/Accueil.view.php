<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Houblon Store - Magasins</title>
    <link rel="stylesheet" type="text/css" href="../view/css/Accueil.view.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Header.view.css">
  </head>
  <body>
    <?php include("../view/Header.view.php") ?>

    <section>
      <h1>Accueil</h1>
      <p>Bienvenue sur le site des magasins Houblon Store ! Vous pourrez retrouver ici l'ensemble de notre sélection de bières artisanales ainsi que tous nos magasins sur le territoire français !
      Bonne visite et à bientôt dans nos magasins !</p>
      <p>L'abus d'alcool est dangereux pour la santé, veuillez consommer avec modération (mais consommez quand même)</p>
    </section>

    <footer>

    </footer>
  </body>
</html>
