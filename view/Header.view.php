<header>
  <a id="logo" href="../controller/Accueil.controller.php"><img src="../view/media/logo.png" alt=""></a>
  <div id="headerContainer">
    <h1>Houblon Store</h1>
    <nav>
      <ul>
        <a href="../controller/Accueil.controller.php"><li>Accueil</li></a>
        <a href="../controller/Magasins.controller.php"><li>Magasins</li></a>
        <a href="../controller/Boutique.controller.php"><li>Boutique</li></a>
      </ul>
    </nav>
  </div>
</header>
