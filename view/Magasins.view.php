<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Houblon Store - Magasins</title>
    <link rel="stylesheet" type="text/css" href="../view/css/Magasins.view.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Header.view.css">
  </head>
  <body>
    <?php include("../view/Header.view.php") ?>

    <section>
      <div id="searchContainer">
        <form action="Magasins.controller.php" method="get">
          <div id="filters">
            <label for="reg">Région :</label>
            <select id="reg" name="region">
              <?php foreach ($regions as $regValue) {
                echo "<option value=\"".$regValue[0]."\">".$regValue[0]."</option>";
              } ?>
            </select>
            <label for="dep">Département :</label>
            <select id="dep" name="departement">
              <?php foreach ($departements as $depValue) {
                echo "<option value=\"".$depValue[0]."\">".$depValue[0]."</option>";
              } ?>
            </select>
            <br>
            <input type="submit" value="Rechercher">
          </div>
        </form>
      </div>
      <div id="resultsContainer">
        <table>
          <tr class="intitules">
            <td>Nom du magasin</td>
            <td>Ville</td>
            <td>Adresse</td>
            <td>Numéro de téléphone</td>
          </tr>
          <?php
            foreach($magasins as $magasin) {
              echo '<tr>';
              echo '<td>'.$magasin->getNom().'</td>';
              echo '<td>'.$magasin->getVille().'</td>';
              echo '<td>'.$magasin->getAdresse().'</td>';
              echo '<td>'.$magasin->getNumTel().'</td>';
              echo '</tr>';
            }
           ?>
        </table>
      </div>
    </section>

    <footer>

    </footer>
  </body>
</html>
