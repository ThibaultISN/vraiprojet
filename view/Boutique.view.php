<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Houblon Store - Boutique</title>
    <link rel="stylesheet" type="text/css" href="../view/css/Boutique.view.css">
    <link rel="stylesheet" type="text/css" href="../view/css/Header.view.css">
  </head>
  <body>
    <?php include("../view/Header.view.php") ?>

    <div id="superContainer">
      <aside>
        <h2>Filtres</h2>
        <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
          <?php foreach ($filters as $filt) {
            echo "<input type=\"checkbox\" id=\"$filt\" name=\"$filt\">";
            echo "<label for=\"$filt\">$filt</label>";
            echo "<br>";
          } ?>
          <p>
            <input type="submit" value="Filtrer">
          </p>
        </form>
      </aside>

      <section>
        <?php foreach ($produits as $prod) {
          echo "<div class=\"produit\">";
          echo "<a href=\"#\"><img src=\"../view/media/".$prod->getRef().".png\" alt=\"\"></a>";
          echo "<p>".$prod->getNom()." - ".$prod->getPrix()."€</p>";
          if (isset($_GET['maListe'])) {
            echo  "<a href=\"?ajoutListe=".$prod->getRef()."&maListe=".$queryMaListe.$queryFilters."\">Ajouter à ma liste</a>";
          } else {
            echo  "<a href=\"?ajoutListe=".$prod->getRef()."&maListe=".$prod->getRef().$queryFilters."\">Ajouter à ma liste</a>";
          }
          echo "</div>";
        } ?>
      </section>

      <aside>
        <h2>Ma liste</h2>
        <ul>
          <?php foreach ($liste as $prod) {
            echo "<li>".$prod->getNom()." - ".(float)($prod->getPrix())."€</li>";
          } ?>
        </ul>
      </aside>
    </div>

    <footer>

    </footer>
  </body>
</html>
